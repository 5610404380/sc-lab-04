package BGUI;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;





public class BankGUI extends JFrame {
		private static final int FRAME_WIDTH = 600;
		private static final int FRAME_HEIGHT = 250;

		private JTextField line;
		private JTextArea show;
		private JButton button;
		private JPanel panel;
		private JPanel panel2;
		private JComboBox combo;
		String style[] = {"Quadrate","Triangle","Switch","Checkerboard"};
	
		
		public void createTextField() {
		    final int FIELD_WIDTH = 10;
		    line = new JTextField(FIELD_WIDTH);
		}
		public void createComboBox(){
			combo = new JComboBox(style);
			combo.setPreferredSize(new Dimension(150,20));
			combo.setSelectedItem("Quadrate");
		}
		public void createTextArea() {
		    show = new JTextArea(10,10);
		}
			
		
		public void createButton(ActionListener listener) {
		    button = new JButton("Show Answer");
		    button.addActionListener(listener);
		}

		public void createPanel1() {
		    panel = new JPanel();
		    panel.add(line);
		    panel.add(combo);
		    panel.add(button);
		    add(panel);
		    
		}
		
		public void createPanel2() {
		    panel2 = new JPanel();
		    panel2.add(show);
		    add(panel2);
		    setVisible(true);
		    setSize(FRAME_WIDTH, FRAME_HEIGHT);
		    
		}
		public String getStringcombo(){
			String select = (String) combo.getSelectedItem();
			return select;
		}
 
		public void setAreashow(String strings) {
			int text = Integer.parseInt(gettext());
			for(int i = 0;i<text;i++){
			show.setText(strings);	
			}
		}
		public String gettext(){
			String text = line.getText();
			return text;
		}
		   
	}

