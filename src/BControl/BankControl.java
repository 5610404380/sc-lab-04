package BControl;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import BGUI.BankGUI;
import BModel.BankModel;



public class BankControl {
	  
	private  BankGUI gui = new BankGUI();
	private  BankModel model;
	
public BankControl() {
		
		model = new BankModel();
		gui.createTextField();
		gui.createTextArea();
		listener = new AddInterestListener();
		gui.createButton(listener);
		gui.createComboBox();
		gui.setLayout(new GridLayout(1,2));
		gui.createPanel1();
		gui.createPanel2();
	}
	
	
	class AddInterestListener implements ActionListener {
    	
		public void actionPerformed(ActionEvent event) {
            gui.getStringcombo();
            gui.setAreashow(model.setanswer(gui.gettext(),gui.getStringcombo()));
            
        }            
    }
	ActionListener listener;
	
	public static void main(String[] args) {
		new BankControl();
		
	}	
	

	
}


	
	
